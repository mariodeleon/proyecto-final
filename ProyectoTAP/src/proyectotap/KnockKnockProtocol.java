package proyectotap;

import java.net.*;
import java.io.*;
import proyectotap.Adivinanza.*;


public class KnockKnockProtocol extends Adivinanza {

    private static final int ESPERA = 0;
    private static final int INICIO = 1;
    private static final int ENVIAR_PISTA = 2;
    private static final int OTRO = 3;
    private static final int NUM_BROMAS = 5;
    private int estado = ESPERA;
    private int contadorRespuesta = 0;

    private String[] pregunta = {"Tengo nombre de animal, cuando la rueda se pincha me tienes que utilizar.",
        "Te la digo y no me entiendes, te la repito y no me comprendes.", 
        "Blanca por dentro,verde por fuera.Si no sabes,espera", "Soy ave y soy llana, pero no tengo pico ni alas.","Redondo soy como un pandero, quien me tome en verano que use sombrero.","En lo alto vive, en lo alto mora, en lo alto teje la tejedora." ,
     "En verano barbudo y en verano desnudo. ¿Qué soy?","Dame de comer y vivo. Dame un trago y me muero", "Si lo escribes como es, \n" +
"soy de la selva el rey. \n" +
"Si lo escribes al revés \n" +
"soy tu Papá Noel.",
    "Puñadito de algodón, brinca sin ton ni son."};
    private String[] respuestas = {"gato", "tela","pera","avellana","sombrero" , "la araña","el bosque", "el fuego","leon", "la oveja"};
    private String[] respuesta = {"Bien!!", "Excelente"};

    public String processInput(String entrada) {
        String salida = null;

        if (estado == ESPERA) {

            salida = "Adivina, adivinador. ¿Estas listo?";
            estado = INICIO;
        } else if (estado == INICIO) {
            if (entrada.equalsIgnoreCase("si")) {
                salida = pregunta[contadorRespuesta];
                estado = ENVIAR_PISTA;
            } else {
                salida = "Se supone que debes responder \"si\"! "
                        + "Otra vez!. Adivina, adivinador. ¿Estas listo?";
            }
        } else if (estado == ENVIAR_PISTA) {
            if (entrada.equalsIgnoreCase(respuestas[contadorRespuesta])) {
                salida = respuesta[0] + " ¿Otra vez? (si/no)";
                estado = OTRO;
            } else {
                salida = "Respuesta incorrecta, intentalo de nuevo. adivina, adivinador. ¿Estas listo?";
                estado = INICIO;
            }
        } else if (estado == OTRO) {
            if (entrada.equalsIgnoreCase("si")) {
                salida = "Adivina, adivinador. ¿Estas listo?";
                if (contadorRespuesta == (NUM_BROMAS - 1)) {
                    contadorRespuesta = 0;
                } else {
                    contadorRespuesta++;
                }
                estado = INICIO;
            } else {
                salida = "FIN";
                
                estado = ESPERA;
            }
        }
        return salida;
    }
}
