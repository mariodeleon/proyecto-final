package proyectotap;

import java.net.*;
import java.io.*;

public class KKMultiServerThread extends Thread {

    private Socket socket = null;

    public KKMultiServerThread(Socket socket) {
        super("KKMultiServerThread");
        this.socket = socket;
    }

    @Override
    public void run() {

        try (
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));) {
                String inputLine, outputLine;
                
                KnockKnockProtocol kkp = new KnockKnockProtocol();
                outputLine = kkp.processInput(null);
                out.println(outputLine);
                
            while ((inputLine = in.readLine()) != null) {
                outputLine = kkp.processInput(inputLine);
                out.println(outputLine);
                
                if (outputLine.equals("Bye")) {
                    break;
                }
            }
            socket.close();
        } catch (IOException e) {
            
            
            
        }
    }
    public static void main(String[] args) {
       String puerto ="9090";
       int portNumber = Integer.parseInt(puerto);
       boolean listening = true;
       try (ServerSocket serverSocket = new ServerSocket(portNumber)) { 
            while (listening) {
                new KKMultiServerThread(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            System.err.println("Error en la conección del puerto." + portNumber);
            
        }
    }
}
