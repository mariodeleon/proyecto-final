package proyectotap;

import java.io.*;
import java.net.*;
import javax.swing.JOptionPane;
 
public class KnockKnockClient {
        String hostName = "192.168.43.13";
        int portNumber = 9090;
       
        public void conectar(){
        
        try (
            Socket kkSocket = new Socket(hostName, portNumber);
            PrintWriter out = new PrintWriter(kkSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(kkSocket.getInputStream()));) {
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
            
            String fromServer;
            String fromUser;
 
            while ((fromServer = in.readLine()) != null) {
                System.out.println("Server: " + fromServer);
                if (fromServer.equals("Bye."))
                    break;
                    System.out.println("knocknockClient");
                 
                fromUser = stdIn.readLine();
                if (fromUser != null) {
                    System.out.println("Client: " + fromUser);
                    out.println(fromUser);
                }
            }
        } catch (UnknownHostException e) {
            JOptionPane.showMessageDialog(null, "No se encuentra el Host");
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "No se encuentra el servidor activo ");
        }
    }
}


