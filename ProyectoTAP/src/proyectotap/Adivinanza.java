package proyectotap;

import java.io.*;
import java.net.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import audio.Audio;


public class Adivinanza extends javax.swing.JFrame {

    private int cont = 0;
    private KnockKnockClient cliente;
    private KnockKnockProtocol protocolo;
    private Audio audio = new Audio();
    public Adivinanza() {
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        
        
        try {
            this.kkSocket = new Socket("localhost", 9090);
            this.in = new BufferedReader(new InputStreamReader(kkSocket.getInputStream()));
            this.out = new PrintWriter(kkSocket.getOutputStream(), true);
            initComponents();
//        getT();
        } catch (IOException ex) {
            Logger.getLogger(Adivinanza.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelNorte = new javax.swing.JPanel();
        campoServidor = new javax.swing.JTextField();
        campoPuerto = new javax.swing.JTextField();
        etiquetaServidor = new javax.swing.JLabel();
        etiquetaPuerto = new javax.swing.JLabel();
        etiquetaConectar = new javax.swing.JLabel();
        panelSur = new javax.swing.JPanel();
        etiquetaEnviarRespuesta = new javax.swing.JLabel();
        etiquetaSalir = new javax.swing.JLabel();
        panelCentro = new javax.swing.JPanel();
        etiquetaPreguntas = new javax.swing.JLabel();
        campoRespuestas = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelNorte.setBackground(java.awt.SystemColor.activeCaptionBorder);
        panelNorte.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        etiquetaServidor.setText("Servidor:");

        etiquetaPuerto.setText("Puerto:");

        etiquetaConectar.setBackground(new java.awt.Color(255, 51, 51));
        etiquetaConectar.setForeground(new java.awt.Color(255, 51, 51));
        etiquetaConectar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/wifi.png"))); // NOI18N
        etiquetaConectar.setText("Conectar");
        etiquetaConectar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        etiquetaConectar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                etiquetaConectarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout panelNorteLayout = new javax.swing.GroupLayout(panelNorte);
        panelNorte.setLayout(panelNorteLayout);
        panelNorteLayout.setHorizontalGroup(
            panelNorteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelNorteLayout.createSequentialGroup()
                .addContainerGap(159, Short.MAX_VALUE)
                .addGroup(panelNorteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(etiquetaServidor, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(etiquetaPuerto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelNorteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(campoServidor, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(campoPuerto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addComponent(etiquetaConectar, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(147, 147, 147))
        );
        panelNorteLayout.setVerticalGroup(
            panelNorteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNorteLayout.createSequentialGroup()
                .addGroup(panelNorteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelNorteLayout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(panelNorteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(etiquetaServidor)
                            .addComponent(campoServidor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panelNorteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(etiquetaPuerto)
                            .addComponent(campoPuerto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelNorteLayout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(etiquetaConectar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        getContentPane().add(panelNorte, java.awt.BorderLayout.PAGE_START);

        panelSur.setBackground(new java.awt.Color(255, 51, 51));

        etiquetaEnviarRespuesta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/charlar.png"))); // NOI18N
        etiquetaEnviarRespuesta.setText("       Enviar                               ");
        etiquetaEnviarRespuesta.setToolTipText("Responder");
        etiquetaEnviarRespuesta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                etiquetaEnviarRespuestaMouseClicked(evt);
            }
        });
        panelSur.add(etiquetaEnviarRespuesta);

        etiquetaSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/aburrido.png"))); // NOI18N
        etiquetaSalir.setText("Salir");
        etiquetaSalir.setToolTipText("¿Te aburriste?");
        etiquetaSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                etiquetaSalirMouseClicked(evt);
            }
        });
        panelSur.add(etiquetaSalir);

        getContentPane().add(panelSur, java.awt.BorderLayout.PAGE_END);

        panelCentro.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.highlight"));
        panelCentro.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        etiquetaPreguntas.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        etiquetaPreguntas.setForeground(new java.awt.Color(255, 51, 51));
        etiquetaPreguntas.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiquetaPreguntas.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        etiquetaPreguntas.setMaximumSize(new java.awt.Dimension(120, 0));
        etiquetaPreguntas.setPreferredSize(new java.awt.Dimension(100, 0));
        etiquetaPreguntas.setVerifyInputWhenFocusTarget(false);
        panelCentro.add(etiquetaPreguntas, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 180, 520, 20));
        panelCentro.add(campoRespuestas, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 200, 130, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagen/xd.jpg"))); // NOI18N
        panelCentro.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, -30, 310, 240));

        getContentPane().add(panelCentro, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public JLabel getEtiquetaPreguntas() {
        return etiquetaPreguntas;
    }

    public JTextField getCampoRespuestas() {
        return campoRespuestas;
    }

    public void setCampoRespuestas(JTextField campoRespuestas) {
        this.campoRespuestas = campoRespuestas;
    }
    public Socket kkSocket;
    public BufferedReader in;
    public PrintWriter out;
    public String fromServer = null;
    
    private void etiquetaSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etiquetaSalirMouseClicked
        
        String a = JOptionPane.showInputDialog(null, "¿Deseas salir?");
        //System.out.println(a);
        
        if (a.equalsIgnoreCase("si")){   
            Login login = new Login();
            login.setVisible(true);
            this.setVisible(false);
            audio.cerrar();
        }
    }//GEN-LAST:event_etiquetaSalirMouseClicked

    private void etiquetaEnviarRespuestaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etiquetaEnviarRespuestaMouseClicked
        try {
            String fromUser = campoRespuestas.getText();
            if (fromServer.equals("Bye.")) {
                System.out.println("imprimo 'Bye'");
                fromUser = campoRespuestas.getText();
                if (fromUser.equalsIgnoreCase("no")) {
                    campoRespuestas.setVisible(false);
                    System.out.println("Advinanza");
                }
            }
            if (fromUser != null) {
                System.out.println("Client: " + fromUser);
                out.println(fromUser);
            }
            System.out.println(fromServer);
            while ((fromServer = in.readLine()) != null) {

                etiquetaPreguntas.setText(fromServer);
                campoRespuestas.setText("");
                break;
            }
        } catch (UnknownHostException e) {
            JOptionPane.showMessageDialog(null, "Error de conexión");

        } catch (IOException ex) {
            Logger.getLogger(Adivinanza.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_etiquetaEnviarRespuestaMouseClicked

    private void etiquetaConectarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_etiquetaConectarMouseClicked
        audio.reproducirAudio();
        
        try {
            Scanner vp = new Scanner(System.in);
            String hostName = campoServidor.getText();
            InetAddress host = InetAddress.getByName(hostName);

            JOptionPane.showMessageDialog(null, "Conectado");
            String fromUser;
            boolean puntero = true;
            while ((fromServer = in.readLine()) != null) {
                etiquetaPreguntas.setText(fromServer);
                break;
            }

        } catch (UnknownHostException e) {
            JOptionPane.showMessageDialog(null, "Error de conexión");

        } catch (IOException ex) {
            Logger.getLogger(Adivinanza.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         campoServidor.setEnabled(false);
         campoPuerto.setEnabled(false);
    }//GEN-LAST:event_etiquetaConectarMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField campoPuerto;
    private javax.swing.JTextField campoRespuestas;
    private javax.swing.JTextField campoServidor;
    private javax.swing.JLabel etiquetaConectar;
    private javax.swing.JLabel etiquetaEnviarRespuesta;
    private javax.swing.JLabel etiquetaPreguntas;
    private javax.swing.JLabel etiquetaPuerto;
    private javax.swing.JLabel etiquetaSalir;
    private javax.swing.JLabel etiquetaServidor;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel panelCentro;
    private javax.swing.JPanel panelNorte;
    private javax.swing.JPanel panelSur;
    // End of variables declaration//GEN-END:variables

    public KnockKnockProtocol getProtocolo() {
        return protocolo;
    }

    public JTextField getCampoPuerto() {
        return campoPuerto;
    }

    public JTextField getCampoServidor() {
        return campoServidor;
    }

    public JLabel getEtiquetaPuerto() {
        return etiquetaPuerto;
    }

    public JLabel getEtiquetaServidor() {
        return etiquetaServidor;
    }

}
