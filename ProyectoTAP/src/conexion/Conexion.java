

package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

public class Conexion {
    
    public static final String URL = "jdbc:mysql://localhost:3306/tap";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "1234";

    public static Connection getConnection() {
        Connection connection = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
//            JOptionPane.showMessageDialog(null, "Conectado");
        } catch (Exception e) {
            System.out.println(e+"aqui");
        }

        return connection;
    }
    
}
